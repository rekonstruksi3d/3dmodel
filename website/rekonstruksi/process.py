import cv2
import numpy as np
import pandas as pd
import plotly.graph_objects as go
from PIL import Image


def process(heightmap_path, texture_path):
    print(texture_path)
    image = cv2.imread(heightmap_path)
    image2 = cv2.imread(texture_path)
    # window_name = 'image'
    # cv2.imshow(window_name, image)

    # img = mpimg.imread(image, cv2.IMREAD_GRAYSCALE)
    scale_percent = 100  # percent of original size
    width = int(image.shape[1] * scale_percent / 100)
    height = int(image.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    resized = cv2.resize(image, dim, interpolation=cv2.INTER_AREA)

    print(image.shape)

    center = image.shape
    x = center[1] - width / 2
    y = center[0] - height / 2



    crop = resized[int(y):int(y + height), int(x):int(x + width)]


    blur = cv2.blur(crop, (1, 1), 0)
    # print(blur)
    array = blur[:, :, 0]
    arrayZ = np.flipud(array)

    # print(lum_img)
    # np.savetxt('dataset/gunung_dataset.csv', lum_img, delimiter=',', fmt='%d')
    #
    # url = 'dataset/gunung_dataset.csv'
    # data = pd.read_csv(url)

    sizeX = ((scale_percent * 20) + 1)
    sizeY = ((scale_percent * 20) + 1)
    arrayX = list(range(1, sizeX))
    arrayY = list(range(1, sizeY))

    resized2 = cv2.resize(image2, dim, interpolation=cv2.INTER_AREA)
    img = resized2[:, :, 1]

    fig = go.Figure(
        go.Surface(
            x=arrayX,
            y=arrayY,
            z=arrayZ,
            hidesurface=False,
            surfacecolor=np.flipud(img),
            # autocolorscale=True,
            # color_dict='rgb'
            # colors='rgb',
            # return_default_colors=False
            colorscale=[[0, 'rgb(0,0,0)'], [1, 'rgb(255,255,255)']],
            # colorscale='Earth',
            showscale=False
        )
    )

    fig.update_layout(
        # title="skala: 1:20",
        scene={
            "xaxis": {"nticks": 10},
            "yaxis": {"nticks": 10},
            "zaxis": {"nticks": 10},
            'camera_eye': {"x": 0, "y": -1, "z": 0.5},
            "aspectratio": {"x": 1, "y": 1, "z": 0.2}
        })
    html = fig.to_html()
    return html
    # fl = open("test.html", "w")
    # fl.write(html)
    # fig.show()
