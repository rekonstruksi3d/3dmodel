import tkinter as tk
from tkinter import filedialog

import cv2
import numpy as np
import pandas as pd
import plotly.graph_objects as go


# class App(tk.Frame):
#     def __init__(self, master):
#         tk.Frame.__init__(self, master)
#         self.pack()
#         self.master.title("Rekonstruksi 3 Dimensi Pegunungan")
#
#         self.master.resizable(False, False)
#         self.master.tk_setPalette(background='#ececec')
#
#         dialog_frame = tk.Frame(self)
#         dialog_frame.pack(padx=10, pady=15)
#
#         tk.Label(dialog_frame, text="Masukan gambar heightmap kemudian klick Process." , font='System 14 bold').pack()
#
#
#
#         button_frame = tk.Frame(self)
#         button_frame.pack(padx=15, pady=(0, 15), anchor='e')
#
#         tk.Button(button_frame, text='Process' , command=self.process).pack(side='right')
#         tk.Button(button_frame, text='Masukan Gambar', command=self.masukanGambar).pack(side='right')


image = cv2.imread('../media/images/merapi.png')
scale_percent = 100  # percent of original size
width = int(image.shape[1] * scale_percent / 100)
height = int(image.shape[0] * scale_percent / 100)
dim = (width, height)
        # resize image
resized = cv2.resize(image, dim, interpolation=cv2.INTER_AREA)

blur = cv2.blur(resized, (5, 5), 0)
print(blur)
lum_img = blur[:, :, 0]

np.savetxt('dataset/gunung_dataset.csv', lum_img, delimiter=',', fmt='%d')

url = 'dataset/gunung_dataset.csv'
data = pd.read_csv(url)

sizeX = ((scale_percent*20)+1)
sizeY = ((scale_percent*20)+1)
arrayX = list(range(1, sizeX))
arrayY = list(range(1, sizeY))

fig = go.Figure(go.Surface(
    x=arrayX,
    y=arrayY,
    z=data))
fig.update_layout(
    scene={
        "xaxis": {"nticks": 10},
        "yaxis": {"nticks": 10},
        "zaxis": {"nticks": 10},
        'camera_eye': {"x": 0, "y": -1, "z": 0.5},
         "aspectratio": {"x": 1, "y": 1, "z": 0.2}
   })
fig.show()


