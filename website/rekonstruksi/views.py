from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, DeleteView
from django.http import HttpResponse
from .models import Post
from .forms import PostForm
from .process import process
from .search import SearchName
from .search_process import search
# from .process import GenerateRekonstruksi
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.shortcuts import redirect
from urllib.parse import urlencode
from urllib.request import urlretrieve


def detail(request, id):
    post = Post.objects.get(id=id)
    return HttpResponse(post.html)

class HomeView(ListView):
    model = Post
    template_name = 'home.html'

class PageDetailView(DetailView):
    model = Post
    template_name = 'details.html'

class SearchView(CreateView):
    model = Post
    form_class = SearchName
    template_name= 'search.html'

    def post(self, request, *args, **kwargs):
        long = float(request.POST['lon'])
        lati = float(request.POST['lat'])
        is_heightMap = request.POST['param-heightMap']

        lon_area1 = (long - 0.04)
        lat_area1 = (lati + 0.04)
        lon_area2 = (long + 0.04)
        lat_area2 = (lati - 0.04)

        # lon_area1 = (long - 0.0904805)
        # lat_area1 = (lati + 0.0904805)
        # lon_area2 = (long + 0.0904805)
        # lat_area2 = (lati - 0.0904805)

        # tampil1 = 'http://terrain.party/api/export?name=heightmap&box={lon_area1},{lat_area1},{lon_area2},{lat_area2}'.format(
        #     lon_area1=lon_area1, lat_area1=lat_area1, lon_area2=lon_area2, lat_area2=lat_area2)
        tampil2 = 'https://api.mapbox.com/styles/v1/mapbox/satellite-v9/static/{long},{lati},14,0.00,0.00/1000x1000@2x?access_token=pk.eyJ1IjoibWFybG82NjYiLCJhIjoiY2trdmR0dDliMXY4eTJ4bnluM2JocXMwYSJ9.cxd7dZ7AyqIT2i8DNAyMJg'.format(
            long=long, lati=lati)

        tampil1 = 'https://tangrams.github.io/heightmapper/#12.89178/{lati}/{long}'.format(
            long=long, lati=lati)

        params = urlencode(dict(access_key="9acd1f4eb014473593ae9b6d5129cdf5",
                                url=tampil1
                                # width=1080
                                ))
        urlretrieve("https://api.apiflash.com/v1/urltoimage?" + params, "../heightmap.jpeg")

        # print(tampil1)
        if is_heightMap:
            return redirect(tampil1)
        else:
            return self.render_to_response({'params': params, 'tampil2': tampil2})
        # return self.render_to_response({'tampil2': tampil2})
        # return HttpResponse(tampil1)


class PostView(CreateView):
    model = Post
    form_class = PostForm
    template_name = 'post.html'


    def post(self, request, *args, **kwargs):
        uploaded_file = request.FILES['image']
        fs = FileSystemStorage()
        save_file = fs.save(uploaded_file.name, uploaded_file)

        uploaded_file2 = request.FILES['texture_map']
        # fs2 = FileSystemStorage()
        save_file2 = fs.save(uploaded_file2.name, uploaded_file2)

        html = process(fs.location+'/'+save_file, fs.location+'/'+save_file2)
        # data = Post(title=request.POST.get('title'), html=html, image=uploaded_file)
        data = Post(title=request.POST.get('title'), html=html)
        data.save()
        # request.POST
        # form = PostForm(request.POST, request.FILES)
        # if form.is_valid(): form.save()
        return HttpResponse(html)


class DeletePostView(DeleteView):
    model = Post
    template_name = 'delete_post.html'
    #fields = '__all__'
    #fields = 'title', 'body'

# image = Post.objects.last().image
# print(image)

# class ProcessView(CreateView):
#     model = Post
#     form_class = GenerateRekonstruksi
#     template_name = 'process.html'


# def home(request):
#     all_data = Post.objects.all
#     return render(request, 'process.html', {'all': all_data})